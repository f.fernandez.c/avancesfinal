import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


class Creador extends React.Component{

    enviarForm(valor, acciones){

        console.log(valor);

        fetch(
            'http://localhost:4000/insertar',
            {
                method:'POST',
                headers:{'Content-type':'application/json',
                          'cors':'Access-Control-Allow-Origin'},
                body:JSON.stringify({   
                    user:{            
                    id_lugar:valor.id_lugar,
                    tipo_lugar: valor.tipo_lugar,
                    nombre_lugar:valor.nombre_lugar,
                    puntuacion:valor.puntuacion,
                    
                    }
                })
            }
        );
    };

    render(){

        let elemento=<Formik 
            
                initialValues={
                    {
                        id_lugar:'',
                        tipo_lugar:'',
                        nombre_lugar:'',
                        puntuacion:'',
                    
                    }
                }

            onSubmit={this.enviarForm}

            validationSchema={ Yup.object().shape(
                {
                    id_lugar: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    tipo_lugar: Yup.string().required('Campo es obligatorio'),
                    nombre_lugar: Yup.string().required('Campo es obligatorio'),
                    puntuacion: Yup.number().typeError('Debe ser un número'),
                    
                }
            )

            }

            >
                <Container className="p-3">
                    <Form>
                        <div className="form-group">
                            <label htmlFor='id_lugar'>ID</label>
                            <Field name="id_lugar" type="text" className="form-control"/>
                            <ErrorMessage name="id_lugar" className="invalid-feedback"></ErrorMessage>
                        </div>

                        <div className="form-group">
                            <label htmlFor='tipo_lugar'>Tipo de lugar</label>
                            <Field name="tipo_lugar" type="text" className="form-control"/>
                            <ErrorMessage name="tipo_lugar" className="invalid-feedback"></ErrorMessage>
                        </div>

                        <div className="form-group">
                            <label htmlFor='nombre_lugar'>Nombre de lugar</label>
                            <Field name="nombre_lugar" type="text" className="form-control"/>
                            <ErrorMessage name="nombre_lugar" className="invalid-feedback"></ErrorMessage>
                        </div>

                        <div className="form-group">
                            <label htmlFor='puntuacion'>Puntuación</label>
                            <Field name="puntuacion" type="text" className="form-control"/>
                            <ErrorMessage name="puntuacion" className="invalid-feedback"></ErrorMessage>
                        </div>


                        <div className="form-group">
                            <Button type="submit" className="btn btn-primary m-4">Aceptar</Button>
                            <Button type="reset" className="btn btn-secondary">Cancelar</Button>
                        </div>
                    </Form>
                </Container>
            </Formik>;

            return elemento;

    };



}

export default Creador;
