
function mundo(){
    var mymap1 = L.map('mymap2',  { 
        fullscreenControl : true , 
        fullscreenControlOptions : { 
          position : 'topleft' 
        } 
      } ).setView([4.554817, -74.096989], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFiaWFuMTAzIiwiYSI6ImNrY3Fyb200OTE2NDgyc28yZ3docmprNHQifQ.rUpVzZzzDd0cM4zrN6rd1A', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token',
    
}).addTo(mymap1);
    var marker = L.marker([4.5211167,-74.0972378]).addTo(mymap1);
    marker.bindPopup("<img src='http://www.ambientebogota.gov.co/image/image_gallery?uuid=13ffee8a-3b22-401c-908d-3130ee201c77&groupId=10157&t=1587055587546' class='imagenes1' >" + "<br>" + "Parque Entrenubes" + "<br> dirección:<br>Calle 50sur,Bogotá" + "<br><a href='../lugares/lugares.html'>Mas información</a> ");
    var circle = L.circle([4.5211167,-74.0972378], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(mymap1);
    var marker=L.marker([4.5640371,-74.101992]).addTo(mymap1);
    marker.bindPopup("<img src='https://www.eltiempo.com/files/article_main/uploads/2018/07/05/5b3ebae4c909c.jpeg' class='imagenes1' >" + "<br>" + "Velodromo" + "<br> dirección:<br>Cra. 5 #19-20 19-20, San Cristóbal, Bogotá" + "<br><a href='../lugares/lugares.html'>Mas información</a> ").openPopup;
    var circle = L.circle([4.5640371,-74.101992], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(mymap1);
    var marker=L.marker([4.5674466,-74.0971965 ]).addTo(mymap1);
    marker.bindPopup("<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Parroquia_El_Divino_Ni%C3%B1o%2C_Bogot%C3%A1_Cund_-_Colombia.jpg/1920px-Parroquia_El_Divino_Ni%C3%B1o%2C_Bogot%C3%A1_Cund_-_Colombia.jpg' class='imagenes1' >" + "<br>" + "Iglesia del Divino Niño" + "<br> dirección:<br>Cra. 5a #28a-18, San Cristóbal, Bogotá" + "<br><a href='../lugares/lugares.html'>Mas información</a> ").openPopup;
    var circle = L.circle([4.5674466,-74.0971965 ], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(mymap1);
    var marker =L.marker([4.5739309,-74.0854494]).addTo(mymap1);
    marker.bindPopup("<img src='https://www.idrd.gov.co/sites/default/files/documentos/san_cristobal.jpg' class='imagenes1' >" + "<br>" + "Parque San Cristobal" + "<br> dirección:<br> San Cristóbal, Bogotá" + "<br><a href='../lugares/lugares.html'>Mas información</a> ").openPopup;
    var circle = L.circle([4.5739309,-74.0854494], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: 500
    }).addTo(mymap1);

 

    document.getElementById('select-location').addEventListener('change', function(e){
    var coords = e.target.value.split(",");
    L.marker(coords).addTo(mymap1)
    mymap1.flyTo(coords, 18);
        });
        
          
}
