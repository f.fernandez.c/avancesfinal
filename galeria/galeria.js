document.addEventListener('DOMContentLoaded',()=>{
    const elementosCarousel = document.querySelectorAll('.carousel')
    M.Carousel.init(elementosCarousel,{
        duration:900,
        dist:-80,
        indicators: true,
        padding:5,
        shift:5,
    });

});
